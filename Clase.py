#!/usr/bin/env python3
# -*- coding:utf-8¡ -*-


class Pelicula():
    def __init__(self):
        self.nombre = ""
        self.year = 0
        self.duracion = ""
        self.descripcion = None
        self.director = ""
        self.metascore = ""
        self.actores = ""

    def get_nombre(self):
        return self.nombre

    def get_genero(self):
        return self.genero

    def get_descripcion(self):
        return self.descripcion

    def get_duracion(self):
        return self.duracion

    def get_director(self):
        return self.director

    def get_metascore(self):
        return self.metascore

    def get_actores(self):
        return self.actores

    def set_nombre(self, nombre):
        if isinstance(nombre, str):
            self.nombre = nombre

    def set_descripcion(self, nombre):
        self.descripcion = nombre

    def set_duracion(self,duracion):
        self.duracion = duracion

    def set_genero(self, nombre):
        self.genero = nombre

    def set_director(self, nombre):
        self.director = nombre

    def set_actores(self, nombre):
        self.actores = nombre

    def set_metascore(self, nombre):
        self.metascore = nombre


