#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import time
import os
import pandas as pd
from pelicula import Pelicula


def menu():
    while True:
        print("¿Que desea hacer?")
        print("1.- Buscar titulos")
        print("2.- Buscar una pelicula")
        print("3.- Buscar por duracion")
        print("4.- Buscar por genero")
        print("5.- Salir")
        opcion = input("Inserte el numero: ")
        if opcion == "1":
            buscador_titulo(peliculas)

        if opcion == "2":
            buscador_pelicula(peliculas)

        if opcion == "3":
            buscador_duracion(peliculas)

        if opcion == "4":
            buscador_genero(peliculas)

        if opcion == "5":
            os.system("clear")
            print("Nos vemos")
            break


def obtener_datos():
    data = pd.read_csv("IMDb movies.csv")
    return data


def pelicula_objeto(movies_2018):
    # Creo lista para guardar las peliculas en objetos
    peliculas = []
    for i, nombre in movies_2018.iterrows():
        # Se van creando los objetos
        obj_pelicula = Pelicula()
        obj_pelicula.set_nombre(nombre["title"].capitalize())
        obj_pelicula.set_descripcion(nombre["description"])
        obj_pelicula.set_duracion(nombre["duration"])
        obj_pelicula.set_genero(nombre["genre"].split(","))
        obj_pelicula.set_director(nombre["director"])
        obj_pelicula.set_actores(nombre["actors"])
        obj_pelicula.set_metascore(nombre["avg_vote"])
        # Se guardan los objetos en la Lista
        peliculas.append(obj_pelicula)
    return peliculas


def buscador_titulo(peliculas):
    # Contador en caso de no encontrar el titulo
    cont = 0
    os.system("clear")
    titulo = input("Ingrese el titulo desea buscar: ")
    print("Buscando peliculas...")
    time.sleep(1.5)
    for i in peliculas:
        # Si el titulo ingresado se encuentra get_nombre imprime el nombre
        if titulo.capitalize() in i.get_nombre():
            print(i.get_nombre())
            cont += 1
    time.sleep(1.5)
    # Titulo no encontrado devuelve al meno
    if cont == 0:
        print("No encontramos ninguna coincidencia")
        time.sleep(1.5)


def buscador_pelicula(peliculas):
    # Contador en caso de no encontrar
    cont = 0
    os.system("clear")
    titulo = input("Ingrese el nombre de la pelicula: ")
    for i in peliculas:
        # Encontrado el nombre de la pelicula
        if titulo.capitalize() == i.get_nombre():
            """ Imprime: nombre, director, genero, minutos, actores, Puntuacion
            y description"""
            print("--------------------")
            print("Nombre:", i.get_nombre(),
                  "Director:", i.get_director(),
                  "\nGenero:", i.get_genero(),
                  "\nDuracion:", i.get_duracion(),
                  "minutos", "\nActores:", i.get_actores(),
                  "\nPuntuacion:", i.get_metascore(),
                  "\nDescripcion:", i.get_descripcion())
            print("--------------------")
            cont += 1
    time.sleep(1.5)
    if cont == 0:
        # Si el nombre no es encontrado devuelve al menu
        print("No encontramos la pelicula")
        time.sleep(1.5)


def buscador_duracion(peliculas):
    # Contador en caso de no encontrado
    cont = 0
    os.system("clear")
    duracion = int(input("duracion de la pelicula en minutos: "))
    print("Bucando peliculas...")
    time.sleep(1.5)
    for i in peliculas:
        # Encontrado una duracion igual a la encontrada imprime el nombre
        if duracion == i.get_duracion():
            cont += 1
            print(i.get_nombre())

    time.sleep(1.5)
    # No encontrado devulve el menu
    if cont == 0:
        print("No hay ninguna pelicula con esta duracion")
        time.sleep(1.5)


def buscador_genero(peliculas):
    # Contador en caso de no encontrado
    cont = 0
    genero_nuevo = []
    # Recorro todos los generos de la peliculas
    for i in peliculas:
        # Los agrego a una lista
        for j in i.get_genero():
            # En algunas generos el genero al comienzo tenia un espacio, strip
            j = j.strip()
            # Se agregan todos los generos
            genero_nuevo.append(j)
    os.system("clear")
    print("Lista de generos: ")
    # Imrime la lista de generos si  repetir para que el usuario escoja
    print(", ".join(set(genero_nuevo)))
    genero = input("Ingrese el genero por el que desea buscar: ")
    print("Buscando peliculas...")
    time.sleep(1.5)
    # Imprime la pelicula en la cual es de ese genero
    for i in peliculas:
        if genero.capitalize() in i.get_genero():
            print(i.get_nombre())
            cont += 1
    time.sleep(1.5)
    if cont == 0:
        # Devuelve al menu si no encuentra el genero
        print("Este genero no existe")
        time.sleep(1.5)


if __name__ == "__main__":
    data = obtener_datos()
    new = data.filter(["title", "genre", "year", "description",
                       "duration", "director", "actors", "avg_vote"])
    movies_2018 = new[new["year"] == 2018]
    peliculas = pelicula_objeto(movies_2018)
    pelicula_objeto(movies_2018)
    menu()
