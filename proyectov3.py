#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import time
import pandas as pd
from pelicula_prueba1 import Pelicula


def menu():
    while True:
        print("¿Que desea hacer?")
        print("1.- Buscar titulos")
        print("2.- Buscar una pelicula")
        print("3.- Buscar por duracion")
        print("4.- Buscar por genero")
        print("5.- Salir")
        opcion = input("Inserte el numero: ")
        if opcion == "1":
            buscador_titulo(peliculas)

        if opcion == "2":
            buscador_pelicula(peliculas)

        if opcion == "3":
            buscador_duracion(peliculas)

        if opcion == "4":
            buscador_genero(peliculas)

        if opcion == "5":
            print("Nos vemos")
            break


def obtener_datos():
    data = pd.read_csv("IMDb movies.csv")
    return data


def pelicula_objeto(movies_2018):
    peliculas = []
    genero_2 = []
    for i, nombre in movies_2018.iterrows():
        obj_pelicula = Pelicula()
        obj_pelicula.set_nombre(nombre["title"].capitalize())
        obj_pelicula.set_descripcion(nombre["description"])
        obj_pelicula.set_duracion(nombre["duration"])
        obj_pelicula.set_genero(nombre["genre"].split(","))
        genero_2.append(obj_pelicula.get_genero())
        obj_pelicula.set_director(nombre["director"])
        obj_pelicula.set_actores(nombre["actors"])
        obj_pelicula.set_metascore(nombre["avg_vote"])
        peliculas.append(obj_pelicula)
    return peliculas, genero_2

def buscador_titulo(peliculas):
    cont = 0
    titulo = input("Ingrese el titulo desea buscar: ")
    print("Buscando peliculas...")
    time.sleep(1.5)
    for i in peliculas:
        if titulo.capitalize() in i.get_nombre():
            print( i.get_nombre())
            cont += 1
    time.sleep(1.5)
    if cont == 0:
        print("No encontramos ninguna coincidencia")
        time.sleep(1.5)


def buscador_pelicula(peliculas):
    cont = 0
    titulo = input("Ingrese el nombre de la pelicula: ")
    for i in peliculas:
        if titulo.capitalize() == i.get_nombre():
            print("--------------------")
            print("Nombre:", i.get_nombre(),
                  "Director:", i.get_director(),
                  "\nGenero:", i.get_genero(),
                  "\nDuracion:", i.get_duracion(),
                  "minutos","\nActores:", i.get_actores(),
                  "\nPuntuacion:", i.get_metascore(),
                  "\nDescripcion:", i.get_descripcion())
            print("--------------------")
            cont += 1
    time.sleep(1.5)
    if cont == 0:
        print("No encontramos la pelicula")
        time.sleep(1.5)


def buscador_duracion(peliculas):
    cont = 0
    duracion = int(input("duracion de la pelicula: "))
    print("Bucando peliculas...")
    time.sleep(1.5)
    for i in peliculas:
        if duracion == i.get_duracion():
            cont += 1
            print(i.get_nombre())

    time.sleep(1.5)
    if cont == 0:
        print("No hay ninguna pelicula con esta duracion")
        time.sleep(1.5)


def buscador_genero(peliculas):
    cont = 0
    print(genero_2)
    genero = input("Ingrese el genero por el que desea buscar: ")
    print("Buscando peliculas...")
    time.sleep(1.5)
    for i in peliculas:
        if genero.capitalize() in i.get_genero():
            print(i.get_nombre())
            cont += 1
    time.sleep(1.5)
    if cont == 0:
        print("Este genero no existe")
        time.sleep(1.5)


if __name__ == "__main__":
    data = obtener_datos()
    new = data.filter(["title", "genre", "year", "description", "duration", "director", "actors", "avg_vote"])
    movies_2018 = new[new["year"] == 2018]
    peliculas, genero_2 = pelicula_objeto(movies_2018)
    pelicula_objeto(movies_2018)
    menu()
