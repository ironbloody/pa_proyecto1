#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import pandas as pd
from titulo import Titulo
import time


def menu():
    while True:
        print("¿Que desea hacer?")
        print("1.- Buscar una pelicula")
        print("2.- Ordenar")
        print("3.- Buscar por año")
        print("4.- Salir")
        opcion = input("Inserte el numero: ")
        if opcion == "1":
            searchmovie()
        if opcion == "3":
            searchyear()
        if opcion == "4":
            break

def searchmovie():
    new = data.filter(["title", "year", "duration", "director"])
    nombre = input("Ingrese el nombre de la pelicula porfavor: ")
    movies_2019 = new[new["title"] == nombre]
    movies_2019.to_csv("movies_2019.csv", sep=";", index=False)
    for index, row in movies_2019.iterrows():
        print("Fue pulbicada el año", row["year"], "tiene una duracion de", row["duration"],
              "minutos y fue dirigida por", row["director"])
    time.sleep(1.0)


def searchyear():
    new = data.filter(["title", "year"])
    year = int(input("Ingrese el año que desa buscar: "))
    movies_2019 = new[new["year"] == year]
    movies_2019.to_csv("movies_2019.csv", sep=";", index=False)
    time.sleep(1.0)
    print("Peliculas producidas en ese año: ")
    for index, row in movies_2019.iterrows():
        print(row["title"])
    time.sleep(1.0)

if __name__ == '__main__':
    data = pd.read_csv("IMDb movies.csv")

    menu()
